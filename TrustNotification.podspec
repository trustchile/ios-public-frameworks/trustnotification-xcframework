Pod::Spec.new do |spec|
    spec.name                         = 'TrustNotification'
    spec.module_name                  = 'TrustNotification'
    spec.version                      = '0.1.20'
    spec.license                      = { :type => "MIT", :file => "LICENSE" }
    spec.homepage                     = 'https://gitlab.com/trustchile/ios-public-frameworks/trustnotification-xcframework'
    spec.authors                      = { 'Benjamin Cáceres' => 'bcaceres@trust.lat' }
    spec.summary                      = 'TrustNotification is a library for managing Video, Dialog and Push notifications.'
    spec.source                       = { :git => 'https://gitlab.com/trustchile/ios-public-frameworks/trustnotification-xcframework', :tag => "#{spec.version}" }

    
    spec.platform                     = :ios
    spec.swift_version                = '5.0'
    spec.ios.deployment_target        = '13.0'
    spec.requires_arc                 = true

    spec.resources                    = 'Library/TrustNotification/**/*.{storyboard,xib,xcdatamodeld}'
    spec.frameworks                   = 'UIKit', 'CoreData'
    spec.resource_bundles             = { 'TrustNotification' => ['Library/TrustNotification/Models/Notification.xcdatamodeld'] }
    spec.vendored_frameworks         = 'TrustNotification.xcframework'

end
