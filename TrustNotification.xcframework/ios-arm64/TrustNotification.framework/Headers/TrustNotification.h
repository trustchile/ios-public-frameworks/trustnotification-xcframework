//
//  TrustNotification.h
//  TrustNotification
//
//  Created by Benjamin Cáceres on 09-12-21.
//

#import <Foundation/Foundation.h>

//! Project version number for Notification.
FOUNDATION_EXPORT double NotificationVersionNumber;

//! Project version string for Notification.
FOUNDATION_EXPORT const unsigned char NotificationVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Notification/PublicHeader.h>
